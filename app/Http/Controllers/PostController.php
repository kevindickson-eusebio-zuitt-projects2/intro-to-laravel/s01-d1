<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Post;

use Auth;


class PostController extends Controller
{
    // Create a new post

    public function __construct(){

    	$this->middleware('auth')->except(['index','show']);
        $this->middleware('is.post.owner')->except(['index','show','create','myPosts','myArchive']);
        // ->except() no need authentication to access.

    }

    // Create a new post

    // Endpoint: GET /posts/create

    public function create(){

    	return view('posts.create');

    }

    // Endpoint: Post /posts

    public function store(Request $req){

    	// Creating a new post object

    	$new_post = new Post([

    		'title' => $req->input('title'),

    		'content' => $req->input('content'),

    		'user_id' => Auth::user()->id

    	]);

    	// Save it to the database

    	$new_post->save();

    	// Redirect the user somewhere

    	return redirect('/posts/create');

    }


    // Endpoint: GET /posts

    public function index(){

    	$post_list = Post::all();

    	return view("posts.index")->with('posts', $post_list);
    	// with-> for passing the identifier to the index.blade.php
    }


    // Endpoint: GET /posts/<post_id>

    public function show($post_id){

        // Retreving a specific post

        $post = Post::find($post_id);

        return view('posts.show')->with('post', $post);
    }

    // Endpoint: GET /posts/my-posts

    public function myPosts(){

        $my_posts = Auth::user()->posts;

        return view("posts.index")->with('posts', $my_posts);
    }

    // Endpoint: GET /posts/<posts_id>/edit

    public function edit($post_id){

        // Find the post to be updated
        $existing_post = Post::find($post_id);

        return view('posts.edit')->with('post',$existing_post);
        // Redirect the user to page whhere the pst will be updated
    }

    // Endpoint: PUT /posts/<posts_id>

    public function update($post_id, Request $req){

        // Finding existing post to be udpated
        $existing_post = Post::find($post_id);

        // Set the new values of an existing post
        $existing_post->title = $req->input('title');
        $existing_post->content = $req->input('content');

        $existing_post->save(); //to save
        // Redirect thee user to the page of individual post

        return redirect("/posts/$post_id");

    }

    // Endpoint: /posts/<posts_id>

   public function destroy($post_id)
    {
        // Find the existing post to be deleted
        $existing_post = Post::find($post_id);
        // Delete the post
        $existing_post->delete();
        // Redirect the user somewhere
        return redirect('/posts');
    }

    // Endpoint: /posts/<post_id>/archive

    public function archive($post_id){


        // Find an existing post to be archived
        $existing_post = Post::find($post_id);

        // Set the new value of the is_active field
        $existing_post->is_active = false;

        $existing_post->save(); //to save

        // Redirect user somewhere
        return redirect("/posts/$post_id");

    }

    // Endpoint: /posts/<post_id>/archive/view

    public function myArchive(){

       $my_posts = Auth::user()->posts;

        return view("posts.viewArchive")->with('posts', $my_posts);

    }
}
