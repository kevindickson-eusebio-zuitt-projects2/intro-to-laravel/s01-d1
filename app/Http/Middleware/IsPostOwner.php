<?php

namespace App\Http\Middleware;

use Closure;

use App\Post;

class IsPostOwner
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // dd($request->user());

        // Retrieve the id of the authenticated user
        $current_user_id = $request->user()->id;

        // Retrieve the post record trying to access
        $current_post = Post::find($request->post_id);

        // Allow access to the route, if and only if the current userID is 

        // equal to the post's userID 
        if ($current_user_id !== $current_post->user_id) {
            return back();
        }

        return $next($request);
    }
}
