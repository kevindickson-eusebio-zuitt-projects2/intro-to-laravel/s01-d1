@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <h1>{{ $post->title }} <small class="badge badge-pill badge-secondary">{{ $post->is_active ? '' : 'archived' }}</small></h1>
        </div>
        <div class="col-md-8">
            <div class="row">
              <div class="card mb-3" style="width:100%;">
                <div class="card-body">
                  <h3 class="card-title">
                    <a href="#"><small>Posted by: {{ $post->user->name }}</small></a>
                  </h3>
                  <p class="card-text">{{ $post->content }}</p>
                  <div class="container">
                    <div class="row">
                      <div class="col-md-6">
                        <a class="btn btn-link" href="/posts#{{ $post->id }}">Back</a>

                      </div>
                      <div class="col-md-6">

                        @if(Auth::check() && (Auth::user()->id === $post->user_id))
                            <div class="row justify-content-end">
                               <a href="/posts/{{ $post->id }}/edit" class="btn btn-primary mx-1">Edit</a>
                              <form method="POST" action="/posts/{{ $post->id }}">

                                @method('DELETE')

                                @csrf
                               
                                <button type="submit" class="btn btn-danger mx-1">Delete</button>
                              </form>
                              <form method="POST" action="/posts/{{ $post->id }}/archive">

                                @method('PUT')

                                @csrf
                               
                                <button type="submit" class="btn btn-warning mx-1">Archive</button>
                              </form>
                            </div>
                        @endif

                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
        </div>
    </div>
</div>
@endsection